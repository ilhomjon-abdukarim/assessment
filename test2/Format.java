package assessment.test2;

public interface Format {
    String[] format(Customer customer);
}
