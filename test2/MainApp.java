package assessment.test2;

public class MainApp {
    /**
     */

    public static void main(String[] args) {

        //  The user is an employee of the store

        Customer employee = new Customer();
        employee.setCustomerName("Alex Andre");
        employee.setCustomerType("employee");
        employee.setTotalBill(250.7);

        for (String receipt : receiptFormat(employee)) {
            System.out.println(receipt);
        }

        //  The user is an affiliate of the store

        Customer affiliate = new Customer();
        affiliate.setCustomerName("Sandra Cisneros");
        affiliate.setCustomerType("affiliate");
        affiliate.setTotalBill(350.4);

        for (String receipt : receiptFormat(affiliate)) {
            System.out.println(receipt);
        }

        //  The user is a standard and user has been a customer for over 2 years

        Customer standard = new Customer();
        standard.setCustomerName("Marat Bigmayev");
        standard.setCustomerType("standard");
        standard.setCustomerPeriod(4);
        standard.setTotalBill(390.0);

        for (String receipt : receiptFormat(standard)) {
            System.out.println(receipt);
        }

        //  The user is a standard and user has been a customer less then 2 years

        Customer standard2 = new Customer();
        standard2.setCustomerName("Ling Zhing");
        standard2.setCustomerType("standard");
        standard2.setCustomerPeriod(1);
        standard2.setTotalBill(590.0);

        for (String receipt : receiptFormat(standard2)) {
            System.out.println(receipt);
        }
    }

    private static String[] receiptFormat(Customer customer) {
        return FormatFactory.make(customer).format(customer);
    }
}