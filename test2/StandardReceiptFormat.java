package assessment.test2;

public class StandardReceiptFormat implements Format {
    @Override
    public String[] format(Customer customer) {
        String[] strings = new String[4];
        strings[0] = "---------- Receipt ----------";
        strings[1] = "Customer Name: " + customer.getCustomerName();
        strings[2] = "Bill: " + customer.getTotalBill();

        if(customer.getCustomerPeriod()>2){
            strings[3] = "After 5% discount : " + customer.getTotalBill() * 0.95;
        }else {
            int dis = (int) (customer.getTotalBill()/100)*5;
            strings[3] = "After "+ dis +"$ discount : " + (customer.getTotalBill() - dis);
        }
        return strings;
    }
}
