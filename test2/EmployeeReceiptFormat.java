package assessment.test2;

public class EmployeeReceiptFormat implements Format {
    @Override
    public String[] format(Customer customer) {
        String[] strings = new String[4];
        strings[0] = "---------- Receipt ----------";
        strings[1] = "Customer Name: " + customer.getCustomerName();
        strings[2] = "Bill: " + customer.getTotalBill();
        String amount  = String.format ("%.2f", customer.getTotalBill() * 0.7);
        strings[3] = "After 30% discount : " + amount;
        return strings;
    }
}
