package assessment.test2;

public class FormatFactory {
    public static Format make(Customer customer) {
        if ("employee".equalsIgnoreCase(customer.getCustomerType())) {
            return new EmployeeReceiptFormat();
        } else if ("affiliate".equalsIgnoreCase(customer.getCustomerType())) {
            return new AffiliateReceiptFormat();
        } else if ("standard".equalsIgnoreCase(customer.getCustomerType())) {
            return new StandardReceiptFormat();
        }

        return new Format() {
            @Override
            public String[] format(Customer customer1) {
                return new String[0];
            }
        };
    }
}
