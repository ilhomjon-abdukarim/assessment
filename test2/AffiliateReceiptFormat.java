package assessment.test2;

public class AffiliateReceiptFormat implements Format {
    @Override
    public String[] format(Customer customer) {
        String[] strings = new String[4];
        strings[0] = "---------- Receipt ----------";
        strings[1] = "Customer Name: " + customer.getCustomerName();
        strings[2] = "Bill: " + customer.getTotalBill();
        String amount  = String.format ("%.2f", customer.getTotalBill() * 0.9);
        strings[3] = "After 10% discount : " + amount;
        return strings;
    }
}
