package assessment.test2;

public class Customer {
    private String customerName;
    private String customerType;
    private int customerPeriod;
    private double totalBill;
    private int discount;

    public String getCustomerType() {
        return customerType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public int getCustomerPeriod() {
        return customerPeriod;
    }

    public void setCustomerPeriod(int customerPeriod) {
        this.customerPeriod = customerPeriod;
    }

    public double getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(double totalBill) {
        this.totalBill = totalBill;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
