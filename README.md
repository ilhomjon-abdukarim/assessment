# Assessment

This repository has two folders which <b>test1</b> and <b>test2</b>. I used simple Java code using some design pattern. I have used IntelliJ IDEA tool.

In test1 folder has code review assessment and test2 folder has Exercise 1.


<b>Exercise 1</b>

I created few classes and interface. For execute project open MainApp.java class and run. If you check output i just print some details which is customer name bill amount and final amout after discound base ond customer type.
There is some data i created. If you want to test your data created following steps:

        Customer employee = new Customer();
        employee.setCustomerName("Alex Andre");
        employee.setCustomerType("employee");
        employee.setTotalBill(250.7);

        for (String receipt : receiptFormat(employee)) {
            System.out.println(receipt);
        }